const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

	firstName:{
		type: String,
		required: [true, 'First name is required']
	},

	lastName:{
		type: String,
		required: [true, 'Last name is required']
	},

	email:{
		type: String,
		required: [true, 'Email is required']
	},

	password:{
		type: String,
		required: [true, 'password is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNo:{
		type: String,
		required: [true, 'Mobile number is required']
	},

	orders: [
	{
		totalAmount:{
			type: Number,
			required: [true, "Amount is required"]
		},
		productId: {
			type: String,
			required: [true, "Product Id is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}

	}
	]
});

module.exports = mongoose.model('User', userSchema);

