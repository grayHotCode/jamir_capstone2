const jwt = require("jsonwebtoken");
const secret = "ProductBookingAPI";


module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})

}


module.exports.verify = (req, res, next) =>{
	// The token is retrieved from the request header
	let token = req.headers.authorization;

	// Token is recieved and not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		// Bearer
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				return res.send({auth: "failed"})
			}else{
				next();
			}
		});
	}else{
		return res.send({auth: "failed"});
	}


}


module.exports.decode = (token) =>{

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return null;
			}else{
				return jwt.decode(token, {complete:true}).payload;
			}

		});

	}else{
		return null;
	}	

}