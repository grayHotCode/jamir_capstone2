const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require("./routes/user");

const productRoutes = require("./routes/product");



const app = express();
const port = process.env.PORT || 4000;


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.wyjct.mongodb.net/product-booking?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

	
	db.on("error", console.error.bind(console, "Connection Error"))	
	db.once('open', () => console.log('Connected to MongoDB'))

	app.use("/users", userRoutes);
	app.use("/products", productRoutes);




app.listen(port, () => console.log(`Server is running at port ${port}`))


// 
// npx kill-port 4000