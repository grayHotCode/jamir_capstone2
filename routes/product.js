const express = require("express");
const router = express.Router();

const productController = require("../controllers/product");

const auth = require("../auth");


//Retrieve all active products
router.get("/", (req, res) => {
	productController.getAllAvailable().then(resultFromController => 
		res.send(resultFromController));
});


//Retrieve single product
router.get("/:productId", (req, res) => {
	console.log(req.params)
	console.log(req.params.productId)

	productController.getProduct(req.params).then(resultFromController => 
		res.send(resultFromController))
});


// Create product (Admin only)
router.post("/", auth.verify, (req, res) => {		
	const userData = auth.decode(req.headers.authorization);

	productController.addProduct(userData, req.body).then(resultFromController => 
		res.send(resultFromController))
});


//Update product information (Admin only)
router.put("/:productId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization);
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController))
})


// Archive product (Admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.archiveProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));	
});



module.exports = router;
