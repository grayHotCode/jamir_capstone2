const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");


//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => 
		res.send(resultFromController))
})

//Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => 
		res.send(resultFromController));
})


//Set user as admin (Admin only)
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	userController.updateUser(user, req.params, req.body).then(resultFromController => res.send(resultFromController))
})


// Creating an order
router.post("/checkout", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	let data = {
		userId: req.body.userId,
		totalAmount: req.body.totalAmount,
		productId: req.body.productId
	}


	if(userData.isAdmin == false) {
		userController.orderItem(data).then(resultFromController => 
		res.send(resultFromController));
	} else {
		res.send(`Not Authorized`)
	}
	
});


// Retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) =>{

	let userData = auth.decode(req.headers.authorization)

if(userData.isAdmin == false){

	userController.getMyOrders({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
	
}else{
	return false
}


		
	

	
});



module.exports = router;
