const Product = require("../models/Product");

const auth = require("../auth");


//Retrieve all active products
module.exports.getAllAvailable = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


//Retrieve single product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		let product = {
			name: result.name,
			description: result.description,
			price: result.price,
			isActive: result.isActive
		}
		return product

	});
};



// Create product (Admin only)
module.exports.addProduct = async (user, reqBody) => {
	if(user.isAdmin == true){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price

		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return ("Not Authorized");
	}
}


//Update product information (Admin only)
module.exports.updateProduct = async (user, reqParams, reqBody) => {
	
if(user.isAdmin == true){

let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	}

	//Syntax:
		//findByIdAndUpdate(document, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
	.then((product, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
	

}else{
	return ("Admin Only");
}


}


// Archive product (Admin only)
module.exports.archiveProduct = async (user, reqParams, reqBody) => {

	if(user.isAdmin){
		let updateActiveField = {
			isActive : reqBody.isActive
		};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField)
	.then((product, error) => {

			// Product not archived
			if (error) {
				return false;

			// Product archived successfully
			} else {
				return true;

			}
		});
	}
	else {
		return (`Admin only`);
	}	
};

