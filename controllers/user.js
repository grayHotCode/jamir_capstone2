const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
		//Syntax:
			//bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
	})

	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return true
		}
	})

}


// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			}
			else {
				return false
			}
		}
	})
}




//Set user as admin (Admin only)
module.exports.updateUser = async (user, reqParams, reqBody) =>{


if(user.isAdmin == true){

	let updateUser = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(reqParams.userId, updateUser)
	.then((user, error) => {
		if(error){
			return false
		}else{
			return true
		}

	});

}else{
	return ("Admin Only");
}


};


// Creating an order

module.exports.orderItem = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then( user =>{

		user.orders.push({totalAmount: data.totalAmount, productId: data.productId});

		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then( product => {
		
		product.purchase.push({userId: data.userId});
		
		return product.save().then((product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})


		if(isUserUpdated && isProductUpdated) {
			return true
		} 

		
		else {
			return false
		}

}


// Retrieve authenticated user's orders

module.exports.getMyOrders = (reqParams) =>{


	return User.findById(reqParams.userId).then(result =>{

		let order = {

			orders: result.orders
		}

		return order
	});
}

